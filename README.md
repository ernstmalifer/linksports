# linksports

This project is generated with [yo angular generator](https://github.com/yeoman/generator-angular)
version 0.11.1.

## Install

[nodejs](http://nodejs.org/) and set node and npm on environment variable
[ruby](httphttps://www.ruby-lang.org/en/)
`gem install sass` for sass/compass

## Before anything else

Run `npm install` and `bower install` 

## Build & development

Run `grunt` for building and `grunt serve` for preview.

## Testing

Running `grunt test` will run the unit tests with karma.
