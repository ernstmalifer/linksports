'use strict';

describe('Service: partners', function () {

  // load the service's module
  beforeEach(module('linksportsApp'));

  // instantiate service
  var partners;
  beforeEach(inject(function (_partners_) {
    partners = _partners_;
  }));

  it('should do something', function () {
    expect(!!partners).toBe(true);
  });

});
