'use strict';

describe('Directive: repeatFinishDirective', function () {

  // load the directive's module
  beforeEach(module('linksportsApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<repeat-finish-directive></repeat-finish-directive>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the repeatFinishDirective directive');
  }));
});
