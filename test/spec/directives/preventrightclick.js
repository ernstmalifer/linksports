'use strict';

describe('Directive: preventRightClick', function () {

  // load the directive's module
  beforeEach(module('linksportsApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<prevent-right-click></prevent-right-click>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the preventRightClick directive');
  }));
});
