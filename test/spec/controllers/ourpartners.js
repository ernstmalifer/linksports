'use strict';

describe('Controller: OurpartnersCtrl', function () {

  // load the controller's module
  beforeEach(module('linksportsApp'));

  var OurpartnersCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    OurpartnersCtrl = $controller('OurpartnersCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
