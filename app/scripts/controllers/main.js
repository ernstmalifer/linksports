'use strict';

/**
 * @ngdoc function
 * @name linksportsApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the linksportsApp
 */
angular.module('linksportsApp')
  .controller('MainCtrl', function ($scope) {
    $('.carousel').carousel()

    $scope.prev = function(){
      $('.carousel').carousel('prev');
    }
    $scope.next = function(){
      $('.carousel').carousel('next');
    }
  });
