'use strict';

/**
 * @ngdoc function
 * @name linksportsApp.controller:AboutusCtrl
 * @description
 * # AboutusCtrl
 * Controller of the linksportsApp
 */
angular.module('linksportsApp')
  .controller('AboutusCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
