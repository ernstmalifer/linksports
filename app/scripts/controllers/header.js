'use strict';

/**
 * @ngdoc function
 * @name linksportsApp.controller:HeaderCtrl
 * @description
 * # HeaderCtrl
 * Controller of the linksportsApp
 */
angular.module('linksportsApp')
  .controller('HeaderCtrl', function ($scope) {
      // console.log(window.location.hash)
    $scope.isActive = function(link) {
      return link == window.location.hash;
    };
  });
