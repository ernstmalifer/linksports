'use strict';

/**
 * @ngdoc function
 * @name linksportsApp.controller:GalleryCtrl
 * @description
 * # GalleryCtrl
 * Controller of the linksportsApp
 */
angular.module('linksportsApp')
  .controller('GalleryCtrl', function ($rootScope, $scope, _, projects) {

    $scope.projects = projects.getProjects();
    $scope.activeProjectId = 1;
    $scope.activeProject = {};

    $scope.setActiveProject = function(projectId){
      $scope.activeProjectId = projectId;
      $scope.activeProject = angular.copy(_.find($scope.projects, function(project){return project.id == $scope.activeProjectId;}))
      // $scope.$apply();
    }

    $('body').on( 'click', '.project-item', function() {
      $scope.setActiveProject($(this).attr('project-id'))
      $scope.$apply();
    });

    $rootScope.$on('repeatfinished', function(event, mass) {
      
      $scope.setActiveProject(1);

      $('.center').slick({
        infinite: false,
        speed: 300,
        slidesToShow: 8,
        slidesToScroll: 8,
        responsive: [
          {
            breakpoint: 1024,
            settings: {
              slidesToShow: 7,
              slidesToScroll: 7
            }
          },
          {
            breakpoint: 600,
            settings: {
              slidesToShow: 5,
              slidesToScroll: 5
            }
          },
          {
            breakpoint: 480,
            settings: {
              slidesToShow: 3,
              slidesToScroll: 3
            }
          }
        ]
      });
    });
        
  });
