'use strict';

/**
 * @ngdoc function
 * @name linksportsApp.controller:OurpartnersCtrl
 * @description
 * # OurpartnersCtrl
 * Controller of the linksportsApp
 */
angular.module('linksportsApp')
  .controller('OurpartnersCtrl', function ($rootScope, $scope, partners) {

    var slider = {};

    $scope.partners = [];
    
    $scope.$on('$viewContentLoaded', function() {

      $scope.partners = [];
     

      partners.getAllPartners().then(function(resp){        

        $scope.partners = resp.data;
        $scope.selectedpartner = 0;
        $scope.selectedproducttype = 0;
        $scope.selectedimageindex = 0;

        $scope.changeSelectedPartnerIndex = function(index){
          $scope.selectedpartner = index;
          $scope.selectedproducttype = 0;
          $scope.selectedimageindex = 0;
          setMagnify();
        }

        $scope.changeSelectedPartnerSelectedProductType = function(index){
          $scope.selectedproducttype = index;
          $scope.selectedimageindex = 0;
          setMagnify();
        }

        $scope.changeSelectedImageIndex = function(index){
          $scope.selectedimageindex = index;
          setMagnify();
        };
        
      });

      $scope.$on('$destroy', function(){
      });

      $rootScope.$on('repeatfinished', function(mass) {
          slider = $('.bxslider').bxSlider({
            mode: 'horizontal',
            speed: 1000,
            minSlides: 5,
            maxSlides: 5,
            slideWidth: 172,
            slideMargin: 0,
            pager: false
          });

          setMagnify();

        });

      function setMagnify(){
        $('.image-link').magnificPopup({
          type: 'image',
          closeBtnInside: false,
          closeOnContentClick: true,
        
          image: {
            verticalFit: true,
            titleSrc: function(item) {
              return item.el.attr('title');
            }
          }});
      };

      
  });
    
      

  });
