'use strict';

/**
 * @ngdoc function
 * @name linksportsApp.controller:ContactusCtrl
 * @description
 * # ContactusCtrl
 * Controller of the linksportsApp
 */
angular.module('linksportsApp')
  .controller('ContactusCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
