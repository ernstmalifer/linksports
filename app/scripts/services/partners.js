'use strict';

/**
 * @ngdoc service
 * @name linksportsApp.partners
 * @description
 * # partners
 * Service in the linksportsApp.
 */
angular.module('linksportsApp')
  .service('partners', function ($http) {
    this.getAllPartners = function(){
      // var partners = [
      //   {
      //     id: 1, name: 'Porter', products: ['Volleyball, Badminton, Tennis System', 'Roll-up Curtains', 'Travelling Mat Movers', 'Wall Protection Mat', 'Physical Education Equipment', 'Beach Volleyball Equipment', 'Wireless Scorebord']
      //   },
      //   {
      //     id: 2, name: 'Plexipave', products: ['Outdoor and Indoor Playground Equipment for Kids and Adults', 'Climber Boulder', 'Wood Ramps', 'Life trail for Senior Citizens', 'Rock Blocks']
      //   },
      //   {
      //     id: 3, name: 'Fair Play', products: ['4-Sided Electronic Scoreboard', 'One-Sided Electronic Scoreboard', 'Dual Timer Shot Clock', 'Information Message (Electronic)', 'Custom made Electronic Scorecard with Video and Advertising Spaces']
      //   },
      //   {
      //     id: 4, name: 'Herculan', products: ['Rubberized Sports Floor', 'Multi-Purpose Rubberized Sports Floor System', 'Track & Field Indoor/Outdoor Sports Floor']
      //   },
      //   {
      //     id: 5, name: 'Hussey', products: ['Folding & Telescopic Bleachers', 'Global Design Structures of Seating arrangement']
      //   },
      //   {
      //     id: 6, name: 'Colorado Time Systems', products: ['Touch Pads for Olympic Size Swimming Pools', 'Timing Device System (Scoreboard, Start System, Printer, etc)']
      //   },
      //   {
      //     id: 7, name: 'Playworld Systems', products: ['Outdoor and Indoor Playground Equipment for Kids and Adults', 'Climber Boulder', 'Wood Ramps', 'Life trail for Senior Citizens', 'Rock Blocks']
      //   },
      //   {
      //     id: 8, name: 'Action Floor Systems LLC', products: ['Action NitroPanel', 'Portable Floor System', 'Unique, Interlocking Panel System', 'Action Cush II Plus', 'ProAction Thrust', 'Variety of Sports Equipment']
      //   }
      // ];

      var partners = [
        {
          id: 1,
          name: 'Herculan',
          logo: 'images/partners/1/logo.jpg',
          description: '<h4><b>Indoor Surfaces</b></h4><br>Herculan indoor systems provide the optimum slide and slip resistance requirel in today\'s most demanding sports. Due to it\'s high mechanical strength, elastic properties of the thickness layer and the excellent wear resistance of their top-layer, these tough and durable sports floor can also be for exhibitions, concerts, speech-days and all kinds of other non-sports functions without extra protection. All Herculan Sports Floors are easy to maintain and can be resurfaced quicky and economically when required.',
          photos: [
            {
              path: 'images/partners/1/photos/1.jpg',
              path_thumb: 'images/partners/1/photos/1_thumb.jpg'
            },
            {
              path: 'images/partners/1/photos/2.jpg',
              path_thumb: 'images/partners/1/photos/2_thumb.jpg'
            },
            {
              path: 'images/partners/1/photos/3.jpg',
              path_thumb: 'images/partners/1/photos/3_thumb.jpg'
            }
          ]
        },
        {
          id: 2,
          name: 'Herculan',
          logo: 'images/partners/1/logo.jpg',
          description: '<h4><b>Indoor Surfaces 1</b></h4><br>Herculan indoor systems provide the optimum slide and slip resistance requirel in today\'s most demanding sports. Due to it\'s high mechanical strength, elastic properties of the thickness layer and the excellent wear resistance of their top-layer, these tough and durable sports floor can also be for exhibitions, concerts, speech-days and all kinds of other non-sports functions without extra protection. All Herculan Sports Floors are easy to maintain and can be resurfaced quicky and economically when required.',
          photos: [
            {
              path: 'images/partners/1/photos/1.jpg',
              path_thumb: 'images/partners/1/photos/1_thumb.jpg'
            },
            {
              path: 'images/partners/1/photos/2.jpg',
              path_thumb: 'images/partners/1/photos/2_thumb.jpg'
            },
            {
              path: 'images/partners/1/photos/3.jpg',
              path_thumb: 'images/partners/1/photos/3_thumb.jpg'
            }
          ]
        },
        {
          id: 3,
          name: 'Herculan',
          logo: 'images/partners/1/logo.jpg',
          description: '<h4><b>Indoor Surfaces</b></h4><br>Herculan indoor systems provide the optimum slide and slip resistance requirel in today\'s most demanding sports. Due to it\'s high mechanical strength, elastic properties of the thickness layer and the excellent wear resistance of their top-layer, these tough and durable sports floor can also be for exhibitions, concerts, speech-days and all kinds of other non-sports functions without extra protection. All Herculan Sports Floors are easy to maintain and can be resurfaced quicky and economically when required.',
          photos: [
            {
              path: 'images/partners/1/photos/1.jpg',
              path_thumb: 'images/partners/1/photos/1_thumb.jpg'
            },
            {
              path: 'images/partners/1/photos/2.jpg',
              path_thumb: 'images/partners/1/photos/2_thumb.jpg'
            },
            {
              path: 'images/partners/1/photos/3.jpg',
              path_thumb: 'images/partners/1/photos/3_thumb.jpg'
            }
          ]
        },
        {
          id: 4,
          name: 'Herculan',
          logo: 'images/partners/1/logo.jpg',
          description: '<h4><b>Indoor Surfaces</b></h4><br>Herculan indoor systems provide the optimum slide and slip resistance requirel in today\'s most demanding sports. Due to it\'s high mechanical strength, elastic properties of the thickness layer and the excellent wear resistance of their top-layer, these tough and durable sports floor can also be for exhibitions, concerts, speech-days and all kinds of other non-sports functions without extra protection. All Herculan Sports Floors are easy to maintain and can be resurfaced quicky and economically when required.',
          photos: [
            {
              path: 'images/partners/1/photos/1.jpg',
              path_thumb: 'images/partners/1/photos/1_thumb.jpg'
            },
            {
              path: 'images/partners/1/photos/2.jpg',
              path_thumb: 'images/partners/1/photos/2_thumb.jpg'
            },
            {
              path: 'images/partners/1/photos/3.jpg',
              path_thumb: 'images/partners/1/photos/3_thumb.jpg'
            }
          ]
        },
        {
          id: 5,
          name: 'Herculan',
          logo: 'images/partners/1/logo.jpg',
          description: '<h4><b>Indoor Surfaces</b></h4><br>Herculan indoor systems provide the optimum slide and slip resistance requirel in today\'s most demanding sports. Due to it\'s high mechanical strength, elastic properties of the thickness layer and the excellent wear resistance of their top-layer, these tough and durable sports floor can also be for exhibitions, concerts, speech-days and all kinds of other non-sports functions without extra protection. All Herculan Sports Floors are easy to maintain and can be resurfaced quicky and economically when required.',
          photos: [
            {
              path: 'images/partners/1/photos/1.jpg',
              path_thumb: 'images/partners/1/photos/1_thumb.jpg'
            },
            {
              path: 'images/partners/1/photos/2.jpg',
              path_thumb: 'images/partners/1/photos/2_thumb.jpg'
            },
            {
              path: 'images/partners/1/photos/3.jpg',
              path_thumb: 'images/partners/1/photos/3_thumb.jpg'
            }
          ]
        },
        {
          id: 6,
          name: 'Herculan',
          logo: 'images/partners/1/logo.jpg',
          description: '<h4><b>Indoor Surfaces</b></h4><br>Herculan indoor systems provide the optimum slide and slip resistance requirel in today\'s most demanding sports. Due to it\'s high mechanical strength, elastic properties of the thickness layer and the excellent wear resistance of their top-layer, these tough and durable sports floor can also be for exhibitions, concerts, speech-days and all kinds of other non-sports functions without extra protection. All Herculan Sports Floors are easy to maintain and can be resurfaced quicky and economically when required.',
          photos: [
            {
              path: 'images/partners/1/photos/1.jpg',
              path_thumb: 'images/partners/1/photos/1_thumb.jpg'
            },
            {
              path: 'images/partners/1/photos/2.jpg',
              path_thumb: 'images/partners/1/photos/2_thumb.jpg'
            },
            {
              path: 'images/partners/1/photos/3.jpg',
              path_thumb: 'images/partners/1/photos/3_thumb.jpg'
            }
          ]
        }
      ];

      // return partners;
      return $http({ method: 'GET',  url: 'partners.json'});
    }
  });
