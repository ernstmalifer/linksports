'use strict';

/**
 * @ngdoc service
 * @name linksportsApp._
 * @description
 * # 
 * Factory in the linksportsApp._
 */
angular.module('linksportsApp')
  .factory('_', function () {

    // Public API here
    return window._;
  });
