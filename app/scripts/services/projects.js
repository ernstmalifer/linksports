'use strict';

/**
 * @ngdoc service
 * @name linksportsApp.projects
 * @description
 * # projects
 * Service in the linksportsApp.
 */
angular.module('linksportsApp')
  .service('projects', function () {
    // AngularJS will instantiate a singleton by calling "new" on this function

    this.getProjects = function(){
      var projects = [
        {
            "id": 1,
            "title": "ACROPOLIS GREENS.jpg",
            "description": "ACROPOLIS GREENS"
        },
        {
            "id": 2,
            "title": "ADDIDAS SPORTS KAMP - BGC yr 2000.JPG",
            "description": "ADDIDAS SPORTS KAMP - BGC yr 2000"
        },
        {
            "id": 3,
            "title": "ARRELLANO UNIVERSITY - TAFT MANILA.JPG",
            "description": "ARRELLANO UNIVERSITY - TAFT MANILA"
        },
        {
            "id": 4,
            "title": "ASIA PACIFIC COLLEGE.JPG",
            "description": "ASIA PACIFIC COLLEGE"
        },
        {
            "id": 5,
            "title": "ATENEO DE MANILA - BLUE EAGLE GYM.JPG",
            "description": "ATENEO DE MANILA - BLUE EAGLE GYM"
        },
        {
            "id": 6,
            "title": "ATENEO DE MANILA - TRACK OVAL.JPG",
            "description": "ATENEO DE MANILA - TRACK OVAL"
        },
        {
            "id": 7,
            "title": "ATENEO DE MANILA - TRACK SCOREBOARD.JPG",
            "description": "ATENEO DE MANILA - TRACK SCOREBOARD"
        },
        {
            "id": 8,
            "title": "AZURE.JPG",
            "description": "AZURE"
        },
        {
            "id": 9,
            "title": "BARANGAY SAN ANTONIO - PASIG CITY.jpg",
            "description": "BARANGAY SAN ANTONIO - PASIG CITY"
        },
        {
            "id": 10,
            "title": "BRITISH SCHOOL MANILA - RUBBERIZED FLOORRING 2006.JPG",
            "description": "BRITISH SCHOOL MANILA - RUBBERIZED FLOORRING 2006"
        },
        {
            "id": 11,
            "title": "BRITSH SCHOOL MANILA - TELESCOPIC BLEACHER - 2006.JPG",
            "description": "BRITSH SCHOOL MANILA - TELESCOPIC BLEACHER - 2006"
        },
        {
            "id": 12,
            "title": "CATHOLIC SCHOOL OF PACITA 009.jpg",
            "description": "CATHOLIC SCHOOL OF PACITA 009"
        },
        {
            "id": 13,
            "title": "CATHOLIC SCHOOL OF PACITA 010.jpg",
            "description": "CATHOLIC SCHOOL OF PACITA 010"
        },
        {
            "id": 14,
            "title": "COACH RYAN GREGORIO.JPG",
            "description": "COACH RYAN GREGORIO"
        },
        {
            "id": 15,
            "title": "DASMARINAS VILLAGE - MAKATI CITY.JPG",
            "description": "DASMARINAS VILLAGE - MAKATI CITY"
        },
        {
            "id": 16,
            "title": "DASMARINAS VILLAGE.JPG",
            "description": "DASMARINAS VILLAGE"
        },
        {
            "id": 17,
            "title": "DELA SALLE ZOBEL - FLOORING.JPG",
            "description": "DELA SALLE ZOBEL - FLOORING"
        },
        {
            "id": 18,
            "title": "DELA SALLE ZOBEL.JPG",
            "description": "DELA SALLE ZOBEL"
        },
        {
            "id": 19,
            "title": "FAR EASTERN UNIVERSITY - DILAMAN ALUMINUM BLEACHERS.JPG",
            "description": "FAR EASTERN UNIVERSITY - DILAMAN ALUMINUM BLEACHERS"
        },
        {
            "id": 20,
            "title": "FAR EASTERN UNIVERSITY - PORTER GOALS.jpg",
            "description": "FAR EASTERN UNIVERSITY - PORTER GOALS"
        },
        {
            "id": 21,
            "title": "FAR EASTERN UNIVERSITY - R. PAPA GYM.JPG",
            "description": "FAR EASTERN UNIVERSITY - R. PAPA GYM"
        },
        {
            "id": 22,
            "title": "GLOBE TELECOM - BGC.JPG",
            "description": "GLOBE TELECOM - BGC"
        },
        {
            "id": 23,
            "title": "GRENMEADOWS.jpg",
            "description": "GRENMEADOWS"
        },
        {
            "id": 24,
            "title": "IMG_0319.JPG",
            "description": "IMG_0319"
        },
        {
            "id": 25,
            "title": "IMG_0774.JPG",
            "description": "IMG_0774"
        },
        {
            "id": 26,
            "title": "IMG_0901.JPG",
            "description": "IMG_0901"
        },
        {
            "id": 27,
            "title": "IMG_1008.JPG",
            "description": "IMG_1008"
        },
        {
            "id": 28,
            "title": "IMG_1033.JPG",
            "description": "IMG_1033"
        },
        {
            "id": 29,
            "title": "IMG_20100101_114058.jpg",
            "description": "IMG_20100101_114058"
        },
        {
            "id": 30,
            "title": "IMG_2131.JPG",
            "description": "IMG_2131"
        },
        {
            "id": 31,
            "title": "IMMACULATE CONCEPTION - SAN JUAN.JPG",
            "description": "IMMACULATE CONCEPTION - SAN JUAN"
        },
        {
            "id": 32,
            "title": "IMMACULATE CONCEPTION BASKETBALL GYM - SAN JUAN.JPG",
            "description": "IMMACULATE CONCEPTION BASKETBALL GYM - SAN JUAN"
        },
        {
            "id": 33,
            "title": "INTERNATIONAL SCHOOL MANILA - PORTER VOLLEYBALL AND BASKETBALL GOALS.JPG",
            "description": "INTERNATIONAL SCHOOL MANILA - PORTER VOLLEYBALL AND BASKETBALL GOALS"
        },
        {
            "id": 34,
            "title": "INTERNATIONAL SCHOOL MANILA - SWIMMING POOL.JPG",
            "description": "INTERNATIONAL SCHOOL MANILA - SWIMMING POOL"
        },
        {
            "id": 35,
            "title": "INTERNATIONAL SCHOOL MANILA - TRACK SCOREBOARD.JPG",
            "description": "INTERNATIONAL SCHOOL MANILA - TRACK SCOREBOARD"
        },
        {
            "id": 36,
            "title": "INTERNATIONAL SCHOOL MANILA -BGC COVERED COURTS.JPG",
            "description": "INTERNATIONAL SCHOOL MANILA -BGC COVERED COURTS"
        },
        {
            "id": 37,
            "title": "ISM TRACK SCOREBOARD 001.jpg",
            "description": "ISM TRACK SCOREBOARD 001"
        },
        {
            "id": 38,
            "title": "JOY ONGKING - PRIVATE RESIDENCIAL.JPG",
            "description": "JOY ONGKING - PRIVATE RESIDENCIAL"
        },
        {
            "id": 39,
            "title": "MAMASITA - INDOOR RUNNING TRACK.jpg",
            "description": "MAMASITA - INDOOR RUNNING TRACK"
        },
        {
            "id": 40,
            "title": "MARALCO - HOME OF THE BOLTS.JPG",
            "description": "MARALCO - HOME OF THE BOLTS"
        },
        {
            "id": 41,
            "title": "MB BASKETBALL GYM - QUEZON CITY.jpg",
            "description": "MB BASKETBALL GYM - QUEZON CITY"
        },
        {
            "id": 42,
            "title": "MERALCO BADMINTON COURTS - PVC FLOORING.JPG",
            "description": "MERALCO BADMINTON COURTS - PVC FLOORING"
        },
        {
            "id": 43,
            "title": "MERALCO SAN PABLO CITY.jpg",
            "description": "MERALCO SAN PABLO CITY"
        },
        {
            "id": 44,
            "title": "mindanao university science and technology.JPG",
            "description": "mindanao university science and technology"
        },
        {
            "id": 45,
            "title": "MORO LORENZO BASKETBALL FLOORING.JPG",
            "description": "MORO LORENZO BASKETBALL FLOORING"
        },
        {
            "id": 46,
            "title": "NATIONAL UNIVERSITY.JPG",
            "description": "NATIONAL UNIVERSITY"
        },
        {
            "id": 47,
            "title": "PHILIPPINE ARENA STADUIM - TRACK OVAL.JPG",
            "description": "PHILIPPINE ARENA STADUIM - TRACK OVAL"
        },
        {
            "id": 48,
            "title": "PROVINCE OF APAYAO - FLOORING.JPG",
            "description": "PROVINCE OF APAYAO - FLOORING"
        },
        {
            "id": 49,
            "title": "RIZAL TECHNOLOGICAL UNIVERSITY - PORTER VOLLEYBALL.JPG",
            "description": "RIZAL TECHNOLOGICAL UNIVERSITY - PORTER VOLLEYBALL"
        },
        {
            "id": 50,
            "title": "RTU-MANDALUYONG.jpg",
            "description": "RTU-MANDALUYONG"
        },
        {
            "id": 51,
            "title": "SACRED HEART  - ATENEO DE CEBU.JPG",
            "description": "SACRED HEART  - ATENEO DE CEBU"
        },
        {
            "id": 52,
            "title": "SAN MATEO RIZAL - BASKETBALL GYM.JPG",
            "description": "SAN MATEO RIZAL - BASKETBALL GYM"
        },
        {
            "id": 53,
            "title": "SGS BASKETBALL GYM - PORTER GOALS.JPG",
            "description": "SGS BASKETBALL GYM - PORTER GOALS"
        },
        {
            "id": 54,
            "title": "TECHNOLOGICAL  INSTITUTE OF THE PHILIPPINES.JPG",
            "description": "TECHNOLOGICAL  INSTITUTE OF THE PHILIPPINES"
        },
        {
            "id": 55,
            "title": "TONDO SPORTS GYM - MANILA.JPG",
            "description": "TONDO SPORTS GYM - MANILA"
        },
        {
            "id": 56,
            "title": "UNIVERSITY OF MINDANAO - DAVAO CITY.jpg",
            "description": "UNIVERSITY OF MINDANAO - DAVAO CITY"
        },
        {
            "id": 57,
            "title": "URDANETA VILLAGE - MAKATI CITY.JPG",
            "description": "URDANETA VILLAGE - MAKATI CITY"
        },
        {
            "id": 58,
            "title": "URDANETA VILLAGE.jpg",
            "description": "URDANETA VILLAGE"
        },
        {
            "id": 59,
            "title": "YNARES CENTER - ANTIPOLO CITY.JPG",
            "description": "YNARES CENTER - ANTIPOLO CITY"
        },
        {
            "id": 60,
            "title": "YNARES SPORTS ARENA - PASIG CITY.jpg",
            "description": "YNARES SPORTS ARENA - PASIG CITY"
        },
        {
            "id": 61,
            "title": "ZANORTE CONVENTION CENTER - DIPOLOG CITY.JPG",
            "description": "ZANORTE CONVENTION CENTER - DIPOLOG CITY"
        }
      ];

      return projects;
    }

  });
