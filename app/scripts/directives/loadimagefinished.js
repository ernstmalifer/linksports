'use strict';

/**
 * @ngdoc directive
 * @name linksportsApp.directive:loadImageFinished
 * @description
 * # loadImageFinished
 */
angular.module('linksportsApp')
  .directive('loadImageFinished', function ($rootScope) {
    return function(scope, element, attrs) {
      if (scope.$last){
        $rootScope.$emit('repeatimagesfinished', []);
      }
    };
  });
