'use strict';

/**
 * @ngdoc directive
 * @name linksportsApp.directive:preventRightClick
 * @description
 * # preventRightClick
 */
angular.module('linksportsApp')
  .directive('preventRightClick', function () {
    return {
                    restrict: 'A',
                    link: function($scope, $ele) {
                        $ele.bind("contextmenu", function(e) {
                            e.preventDefault();
                        });
                    }
                };
  });

$( document ).on( "contextmenu", "img", function(e) {
    console.log("ypw");
    e.preventDefault();
});