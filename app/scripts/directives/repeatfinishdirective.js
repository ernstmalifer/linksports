'use strict';

/**
 * @ngdoc directive
 * @name linksportsApp.directive:repeatFinishDirective
 * @description
 * # repeatFinishDirective
 */
angular.module('linksportsApp')
  .directive('repeatFinishDirective', function ($rootScope) {
    return function(scope, element, attrs) {
      if (scope.$last){
        console.log("yow");
        $rootScope.$emit('repeatfinished', []);
      }
    };
  });
