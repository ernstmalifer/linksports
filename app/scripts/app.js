'use strict';

/**
 * @ngdoc overview
 * @name linksportsApp
 * @description
 * # linksportsApp
 *
 * Main module of the application.
 */
angular
  .module('linksportsApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ])
  .config(function ($routeProvider, $locationProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .when('/gallery', {
        templateUrl: 'views/gallery.html',
        controller: 'GalleryCtrl'
      })
      .when('/ourpartners', {
        templateUrl: 'views/ourpartners.html',
        controller: 'OurpartnersCtrl'
      })
      .when('/aboutus', {
        templateUrl: 'views/aboutus.html',
        controller: 'AboutusCtrl'
      })
      .when('/contactus', {
        templateUrl: 'views/contactus.html',
        controller: 'ContactusCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });

      $locationProvider.html5Mode(false).hashPrefix('');
  });

